const csv = require('csv-parser')
const fs = require('fs')
const moment = require('moment');

class TransitReport {
  constructor(trips) {
    this.trips = trips;
  }

  get montlyReports() {
    const reports = [];

    for (let month = 0; month < 12; month++) {
      reports.push(new MonthlyReport(month, this.trips.monthly(month)));
    }

    return reports;
  }

  print() {
    this.montlyReports.forEach(report => report.print());
  }
}

class MonthlyReport {
  constructor(month, trips) {
    this.month = month;
    this.trips = trips;
  }

  print() {
    console.log('********');
    this.printHeader();
    this.trips.print();
  }

  printHeader() {
    console.log(
      `Month: ${moment().month(this.month).format('MMMM')}\t`,
      `Trips: ${this.trips.count}\t`,
      `Fare: $${this.trips.fare.toFixed(2)}\t`,
    );
  }
}

class TripsParser {
  constructor() {
    this.trips = new Trips();
    this.tripStart = null;
    this.tripEnd = null;
  }

  parse(data) {
    if (data.transitAgency !== 'Go Transit') return;
    if (data.isFarePayment === false) return;
    // if (data.isDebit === false) return;

    if (!this.tripStart) {
      this.tripStart = data;
      return;
    }

    this.tripEnd = data;

    if (this.tripDurationValid()) {
      this.makeTrip();
    } else {
      this.makeTripWithNoTapOff();
    }
  }

  tripDurationValid() {
    const validTripEndDate = this.tripStart.moment.clone().add(3, 'h');
    return this.tripEnd.moment.isBetween(this.tripStart.moment, validTripEndDate);
  }

  makeTrip() {
    const trip = new Trip();
    trip.start = this.tripStart;
    trip.end = this.tripEnd;
    this.trips.add(trip);

    this.tripStart = null;
    this.tripEnd = null;
  }

  makeTripWithNoTapOff() {
    const trip = new Trip();
    trip.start = this.tripStart;
    this.trips.add(trip);

    this.tripStart = this.tripEnd;
    this.tripEnd = null;
  }
}

class Trips {
  constructor(trips) {
    this.trips = trips || [];
  }

  add(trip) {
    this.trips.push(trip);
  }

  monthly(month) {
    return new Trips(this.trips.filter(trip => trip.month === month));
  }

  get count() {
    return this.trips.length;
  }

  get fare() {
    return this.trips.length > 0
      ? this.trips.map(trip => trip.fare).reduce((total, current) => total + current)
      : 0;
  }

  print() {
    this.trips.forEach(trip => trip.print());
  }
}

class Trip {
  get month() {
    return this.start.moment.month();
  }

  get date() {
    return this.start.moment.format('MM DD YY');
  }

  get time() {
    const timeFormat = 'hh:mm A';
    const startTime = this.start.moment.format(timeFormat);
    const endTime = this.end ? this.end.moment.format(timeFormat) : '!MTO!';
    return `${startTime} - ${endTime}`;
  }

  get fare() {
    const tapOnFare = this.start.fare;
    const tapOffFare = this.end ? this.end.fare : 0;
    return tapOnFare + tapOffFare;
  }

  print() {
    console.log(
      this.date,
      '\t',
      this.time,
      '\t',
      `$${this.fare.toFixed(2)}`,
    );
  }
}

class TripData {
  constructor(data) {
    this.data = data;
  }

  get moment() {
    return moment(this.data['Date'], 'M/D/YYY h:m:s A');
  }

  get transitAgency() {
    return this.data['Transit Agency'];
  }

  get isFarePayment() {
    return this.data['Type'] === 'Fare Payment';
  }

  get isDebit() {
    return this.amount.startsWith('(') && this.amount.endsWith(')');
  }

  get amount() {
    return this.data['Amount'];
  }

  get fare() {
    let fareAmount = this.amount;

    if (this.isDebit) {
      fareAmount = fareAmount.substring(1, fareAmount.length - 2);
    }

    if (fareAmount.startsWith('$')) {
      fareAmount = fareAmount.substring(1);
    }

    return Number.parseFloat(fareAmount) * (this.isDebit ? 1.0 : -1.0);
  }
}

const tripsParser = new TripsParser();

fs.createReadStream('path/to/csv')
.pipe(csv())
.on('data', (data) => {
    tripsParser.parse(new TripData(data));
})
.on('finish', () => new TransitReport(tripsParser.trips).print());



